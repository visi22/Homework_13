
import Foundation

struct Post: Codable {
    var results: [Results]
}

struct Results: Codable {
    let byLine: String
    let type: String
    let abstract: String
    let update: String
    let source: String
    
    enum CodingKeys: String, CodingKey {
        case byLine = "byline"
        case type
        case abstract
        case update = "updated"
        case source
    }
}


