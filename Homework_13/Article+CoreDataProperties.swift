//
//  Article+CoreDataProperties.swift
//  Homework_13
//
//  Created by Andrew Kvasha on 08.08.2022.
//
//

import Foundation
import CoreData


extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var byLine: String?
    @NSManaged public var type: String?
    @NSManaged public var abstract: String?
    @NSManaged public var update: String?
    @NSManaged public var source: String?

}

extension Article : Identifiable {

}
