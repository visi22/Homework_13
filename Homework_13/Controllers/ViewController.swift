
import UIKit

protocol ViewControllerDelegate: AnyObject {
    func didChangeArticle(_ index: Int)
}

class ViewController: UIViewController, ViewControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let decoder = JSONDecoder()
    var articl: Post?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        guard let url = URL(string: "https://api.nytimes.com/svc/mostpopular/v2/emailed/30.json?api-key=hjUrMwAFAFL22J6EeUGrnsZTDLtESG0S") else { return }

        URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in

            guard let strongSelf = self else { return }
            
            guard let data = data else { return }
            
            if error == nil {
                guard let articles: Post = try? strongSelf.decoder.decode(Post.self, from: data) else { return }
                strongSelf.articl = articles
                
                DispatchQueue.main.async {
                    strongSelf.tableView.reloadData()
                }
                
            } else {
                print(error)
            }
            
        }.resume()
        
    }
}

extension ViewController:  UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = articl?.results.count else { return 0 }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        configure(cell: &cell, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        let byline = articl?.results[indexPath.row].byLine
        let type = articl?.results[indexPath.row].type
        let abstract = articl?.results[indexPath.row].abstract
        let update = articl?.results[indexPath.row].update
        let source = articl?.results[indexPath.row].source
        let index = String(indexPath.row)
        var detail = [String]()
        detail.append(byline!)
        detail.append(type!)
        detail.append(abstract!)
        detail.append(update!)
        detail.append(source!)
        detail.append(index)
        
        
        performSegue(withIdentifier: "detaiIdentifier", sender: detail)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detaiIdentifier", let detail = sender as? [String] {
            let destController = segue.destination as! DetailViewController
            destController.array = detail
            destController.delegate = self
        }
    }
    
    private func configure(cell: inout UITableViewCell, for indexPath: IndexPath) {
        var configuration = cell.defaultContentConfiguration()
        configuration.text = articl?.results[indexPath.row].type
        configuration.secondaryText = articl?.results[indexPath.row].byLine
        cell.contentConfiguration = configuration
    }
    func didChangeArticle(_ index: Int) {
        
        self.articl?.results.remove(at: index)
        tableView.reloadData()
    }

}




