import UIKit
import CoreData


class DetailViewController: UIViewController {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var byLabel: UILabel!
    @IBOutlet weak var abstractLabel: UILabel!
    @IBOutlet weak var updateLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    
    weak var delegate: ViewControllerDelegate?
    var array: [String]!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        byLabel.text = array[0]
        typeLabel.text = array[1]
        abstractLabel.text = array[2]
        updateLabel.text = array[3]
        sourceLabel.text = array[4]

        byLabel.sizeToFit()
        typeLabel.sizeToFit()
        abstractLabel.sizeToFit()
        updateLabel.sizeToFit()
        sourceLabel.sizeToFit()
    }
    
    @IBAction func addArticleToFavourite(_ sender: Any) {
        
        delegate?.didChangeArticle(Int(array[5]) ?? 5)
        array.remove(at: 5)
        let newArticle = Article(context: self.context)
        newArticle.byLine = array[0]
        newArticle.type = array[1]
        newArticle.abstract = array[2]
        newArticle.update = array[3]
        newArticle.source = array[4]
        
        do {
            try self.context.save()
        }
        catch {
        }
    }
}
    
    


