

import UIKit
import CoreData


class FavouriteArticlesViewController: UIViewController {

    
    @IBOutlet weak var favouritTableView: UITableView!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var data: [Article]?
    
    override func viewWillAppear(_ animated: Bool) {
        fetchArticle()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        favouritTableView?.dataSource = self
        favouritTableView?.delegate = self
        fetchArticle()
    }
    
    func fetchArticle() {
        do {
            self.data = try context.fetch(Article.fetchRequest())
            
            DispatchQueue.main.async {
                self.favouritTableView?.reloadData()
            }
        }
        catch {
        }
    }
}


extension FavouriteArticlesViewController:  UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = data?.count else { return 0 }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "favouritCellIdentifier", for: indexPath)
        configure(cell: &cell, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.favouritTableView.deselectRow(at: indexPath, animated: true)
        let byline = data?[indexPath.row].byLine
        let type = data?[indexPath.row].type
        let abstract = data?[indexPath.row].abstract
        let updated = data?[indexPath.row].update
        let source = data?[indexPath.row].source
        var detail = [String]()
        detail.append(byline!)
        detail.append(type!)
        detail.append(abstract!)
        detail.append(updated!)
        detail.append(source!)
    
        performSegue(withIdentifier: "favouritIdentifier", sender: detail)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "favouritIdentifier", let detail = sender as? [String] {
            let destController = segue.destination as! FavouritVC
            destController.array = detail
        }
    }
    
    private func configure(cell: inout UITableViewCell, for indexPath: IndexPath) {
        var configuration = cell.defaultContentConfiguration()
        configuration.text = data?[indexPath.row].type
        configuration.secondaryText = data?[indexPath.row].byLine
        cell.contentConfiguration = configuration
    }
    
}

